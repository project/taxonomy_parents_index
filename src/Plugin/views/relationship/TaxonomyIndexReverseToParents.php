<?php

namespace Drupal\taxonomy_parents_index\Plugin\views\relationship;

use Drupal\views\Plugin\views\relationship\RelationshipPluginBase;
use Drupal\views\Plugin\ViewsHandlerManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Relationship handler for the taxonomy_parents_index table.
 *
 * It can be used for joining with the core taxonomy_index table.
 *
 * @ingroup views_relationship_handlers
 *
 * @ViewsRelationship("taxonomy_index_reverse_to_parents")
 */
class TaxonomyIndexReverseToParents extends RelationshipPluginBase {

  /**
   * Join manager.
   *
   * @var \Drupal\views\Plugin\ViewsHandlerManager
   */
  protected $joinManager;

  /**
   * TaxonomyIndexReverseToParents constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\views\Plugin\ViewsHandlerManager $join_manager
   *   Join manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ViewsHandlerManager $join_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->joinManager = $join_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.views.join')
    );
  }

  /**
   * Called to implement a relationship in a query.
   */
  public function query() {
    // Get the current table alias.
    $this->ensureMyTable();

    $leftFieldName = $this->definition['left_field'];
    $baseTableName = $this->definition['base'];

    $joinConfiguration = [
      'left_table' => $this->tableAlias,
      'left_field' => $leftFieldName,
      // Database table to join.
      'table' => $baseTableName,
      // Database field to join on.
      'field' => $this->definition['base field'],
      'adjusted' => TRUE,
    ];

    // Setting LEFT or INNER JOIN based on the views' settings.
    if (!empty($this->options['required'])) {
      $joinConfiguration['type'] = 'INNER';
    }

    // Set join handler plugin.
    if (!empty($def['join_id'])) {
      $id = $def['join_id'];
    }
    else {
      $id = 'standard';
    }
    $join = $this->joinManager->createInstance($id, $joinConfiguration);
    $join->adjusted = TRUE;

    // Use a short alias for this.
    $alias = $leftFieldName . '_' . $this->table;

    // Add relationship to the query.
    $this->alias = $this->query->addRelationship($alias, $join, $baseTableName, $this->relationship);
  }

}
