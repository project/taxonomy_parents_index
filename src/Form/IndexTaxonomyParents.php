<?php

namespace Drupal\taxonomy_parents_index\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds a form where the Taxonomy Term parent ID table can be rebuilt.
 *
 * @package Drupal\taxonomy_parents_index\Form
 */
class IndexTaxonomyParents extends FormBase implements ContainerInjectionInterface {

  /**
   * IndexTaxonomyParents constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service.
   * @param \Drupal\Core\StringTranslation\TranslationManager $stringTranslation
   *   String translation.
   */
  public function __construct(MessengerInterface $messenger, TranslationManager $stringTranslation) {
    $this->messenger = $messenger;
    $this->stringTranslation = $stringTranslation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('messenger'),
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'taxonomy_parents_index_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['helptext'] = [
      '#type' => 'item',
      '#markup' => $this->t('Rebuilds the index table in the database. Caution: it may take a while.'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reindex'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $termCount = self::getTermCount();

    if ($termCount === 0) {
      $this->messenger->addMessage($this->t('There are no taxonomy terms.'));
      return;
    }

    $batch = [
      'title' => 'Indexing all parents for Taxonomy Terms',
      'finished' => 'Drupal\taxonomy_parents_index\Form\IndexTaxonomyParents::batchFinished',
      'operations' => [
        0 => [
          'Drupal\taxonomy_parents_index\Form\IndexTaxonomyParents::indexTerms',
          [],
        ],
      ],
    ];

    batch_set($batch);

    $this->messenger->addMessage($this->t('Reindexing has been completed.'));
  }

  /**
   * Returns the Taxonomy Term count.
   *
   * @return int
   *   Taxonomy Term count.
   */
  public static function getTermCount() {
    $result = \Drupal::database()
      ->select('taxonomy_term_data', 't')
      ->fields('t', ['tid'])
      ->countQuery()
      ->execute()
      ->fetchField();
    return ($result === FALSE) ? 0 : $result;
  }

  /**
   * Implements the batch process for reindexing Taxonomy Term parents.
   *
   * @param string[] $context
   *   Variables for the batch process.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function indexTerms(array &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox'] = [];
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['current_id'] = 0;
      $context['sandbox']['max'] = self::getTermCount();
    }

    $termStorage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    $limit = 100;
    $query = $termStorage->getQuery('AND');
    $query->condition('tid', $context['sandbox']['current_id'], '>');
    $query->sort('tid', 'ASC');
    $query->accessCheck(FALSE);
    $query->range(0, $limit);
    $terms = $query->execute();

    /** @var \Drupal\taxonomy_parents_index\TaxonomyParentsIndexManager $customTaxonomyParentsIndexManager */
    $customTaxonomyParentsIndexManager = \Drupal::service('taxonomy_parents_index.manager');
    foreach ($terms as $revisionId => $termId) {
      $customTaxonomyParentsIndexManager->indexAncestorTerms($termId, FALSE);

      $context['sandbox']['current_id'] = $termId;
      $context['message'] = t('Indexing parents for Taxonomy Term: @id / @maxId', [
        '@id' => $termId,
        '@maxId' => $context['sandbox']['max'],
      ]
      );
    }

    // In case any of the entities fail to load, increase the progress by the
    // stub entity count.
    $context['sandbox']['progress'] += count($terms);

    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = ($context['sandbox']['progress'] >= $context['sandbox']['max']);
    }
  }

  /**
   * Shows messages for the user when the reindexing batch process finished.
   *
   * @param bool $success
   *   Indicate that the batch tasks were all completed successfully.
   * @param array $results
   *   An array of all the results (not used in this function).
   * @param array $operations
   *   A list of all the operations (not used in this function).
   */
  public static function batchFinished($success, $results, $operations) {
    $messenger = \Drupal::messenger();

    if ($success) {
      $message = t('Reindexing has been completed.');
      $messenger->addMessage($message);
    }
    else {
      $message = t('There were errors during the process. Try again later.');
      $messenger->addError($message);
    }

    /** @var \Drupal\taxonomy_parents_index\TaxonomyParentsIndexManager $customTaxonomyParentsIndexManager */
    $customTaxonomyParentsIndexManager = \Drupal::service('taxonomy_parents_index.manager');
    $customTaxonomyParentsIndexManager->invalidateTaxonomyTermCacheTags();
  }

}
