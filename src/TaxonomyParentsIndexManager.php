<?php

namespace Drupal\taxonomy_parents_index;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\taxonomy\TermInterface;

/**
 * Implements indexing in the taxonomy_parents_index table.
 */
class TaxonomyParentsIndexManager extends ControllerBase {

  /**
   * Taxonomy term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * TaxonomyParentsIndexManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Taxonomy term storage.
   * @param \Drupal\Core\Database\Connection $connection
   *   Database connection.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cacheTagsInvalidator
   *   Cache tags invalidator.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    Connection $connection,
    CacheTagsInvalidatorInterface $cacheTagsInvalidator
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->connection = $connection;
    $this->cacheTagsInvalidator = $cacheTagsInvalidator;
  }

  /**
   * Returns the Taxonomy Term storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface|\Drupal\taxonomy\TermStorageInterface
   *   Taxonomy Term storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTermStorage() {
    if (!$this->termStorage) {
      $this->termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    }
    return $this->termStorage;
  }

  /**
   * Indexes parents of a Term.
   *
   * @param int $tid
   *   Taxonomy Term ID.
   * @param bool $invalidateCacheTagsOnEnd
   *   Stores if the Term's cache tag should be invalidated after the process.
   *
   * @throws \Exception
   */
  public function indexAncestorTerms($tid, $invalidateCacheTagsOnEnd = TRUE) {
    // Remove all previously indexed rows.
    $this->deleteAllTermIndex($tid);

    $allParentTermIds = $this->getAllSzulo($tid, TRUE);

    // Write the parent Term IDs to the database table.
    $query = $this->connection->insert('taxonomy_parents_index');

    $query->fields([
      'tid',
      'ptid',
    ]);

    foreach ($allParentTermIds as $parentId) {
      $query->values([
        'tid' => $tid,
        'ptid' => $parentId,
      ]);
    }

    $query->execute();

    if ($invalidateCacheTagsOnEnd) {
      $this->invalidateTaxonomyTermCacheTags();
    }
  }

  /**
   * Remove all previously indexed parent Term IDs to a given Term ID.
   *
   * @param int $tid
   *   Taxonomy Term ID.
   */
  protected function deleteAllTermIndex($tid) {
    $query = $this->connection->delete('taxonomy_parents_index');
    if (is_array($tid)) {
      $query->condition('tid', $tid, 'IN');
    }
    else {
      $query->condition('tid', $tid);
    }
    $query->execute();
  }

  /**
   * Remove all previously indexed Term IDs to a given parent Term ID.
   *
   * @param int $tid
   *   A parent Taxonomy Term ID.
   */
  protected function deleteAllTermParentIndex($tid) {
    $query = $this->connection->delete('taxonomy_parents_index');
    if (is_array($tid)) {
      $query->condition('ptid', $tid, 'IN');
    }
    else {
      $query->condition('ptid', $tid);
    }
    $query->execute();
  }

  /**
   * Remove all previously indexed rows where the Term ID is present.
   *
   * @param int $tid
   *   The Taxonomy Term ID.
   */
  protected function deleteAllTermWithParentIndex($tid) {
    $query = $this->connection->delete('taxonomy_parents_index');
    if (is_array($tid)) {
      $db_or = new Condition("OR");
      $db_or->condition('tid', $tid, 'IN');
      $db_or->condition('ptid', $tid, 'IN');
    }
    else {
      $db_or = new Condition("OR");
      $db_or->condition('tid', $tid, '=');
      $db_or->condition('ptid', $tid, '=');
    }
    $query->condition($db_or);
    $query->execute();
  }

  /**
   * Returns every parent of a Term ID as an array.
   *
   * @param int $tid
   *   Taxonomy Term ID.
   * @param bool $asIdsArray
   *   If TRUE, then the result is a Term ID array.
   *   If FALSE, then the result is an array of Term objects.
   *
   * @return array|\Drupal\taxonomy\TermInterface[]
   *   Result array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getAllSzulo($tid, $asIdsArray = FALSE) {
    $parents = $this->getTermStorage()->loadAllParents($tid);
    if (!$asIdsArray) {
      return $parents;
    }
    return array_keys($parents);
  }

  /**
   * Remove all previously indexed rows where the Term ID is present.
   *
   * If it has children Terms, the Drupal will remove them.
   * Here we need to remove only those rows that belong to the given Term's ID,
   * plus every row where the given Term's ID is in the ptid column.
   *
   * @param \Drupal\taxonomy\TermInterface|null $term
   *   The given Taxonomy Term ID.
   * @param bool $invalidateCacheTagsOnEnd
   *   Sets if it should invalidate cache tags or not.
   */
  public function removeTermIndex(?TermInterface $term, $invalidateCacheTagsOnEnd = TRUE) {
    if (empty($term)) {
      return;
    }
    $tid = $term->id();
    $this->deleteAllTermWithParentIndex($tid);

    if ($invalidateCacheTagsOnEnd) {
      $this->invalidateTaxonomyTermCacheTags();
    }
  }

  /**
   * Invalidates the 'taxonomy_term_list' cache tag.
   */
  public function invalidateTaxonomyTermCacheTags() {
    $this->cacheTagsInvalidator->invalidateTags(['taxonomy_term_list']);
  }

  /**
   * Updates indexes that belong to a given Taxonomy Term.
   *
   * @param \Drupal\taxonomy\TermInterface|null $term
   *   The given Taxonomy Term ID.
   * @param bool $invalidateCacheTagsOnEnd
   *   Sets if it should invalidate cache tags or not.
   *
   * @throws \Exception
   */
  public function updateTermIndex(?TermInterface $term, $invalidateCacheTagsOnEnd = TRUE) {
    if (empty($term)) {
      return;
    }

    if (!$term->isDefaultRevision()) {
      return;
    }

    // If the term didn't change, do nothing.
    $originalTerm = $term->original;
    $newParent = $term->get('parent')->getValue();
    $oldParent = $originalTerm->get('parent')->getValue();
    if (isset($newParent[0]['target_id']) &&
      isset($oldParent[0]['target_id']) &&
      $oldParent[0]['target_id'] == $newParent[0]['target_id']) {
      return;
    }

    // If the parent has changed, need to reindex:
    // - The modified Taxonomy Term,
    // - All of the Term's child Terms.
    $tid = $term->id();
    $allChildren = $this->getAllChildren($term);

    $termsToReindex = array_unique(
      array_merge(
        [$tid => $tid],
        $allChildren
      )
    );

    foreach ($termsToReindex as $termToReindex) {
      $this->indexAncestorTerms($termToReindex, FALSE);
    }
    if ($invalidateCacheTagsOnEnd) {
      $this->invalidateTaxonomyTermCacheTags();
    }
  }

  /**
   * Returns currently indexed parent Term IDs for the given Term ID.
   *
   * @param int $tid
   *   Term ID.
   *
   * @return int[]
   *   Array of parent Taxonomy Term IDs. Empty array if there's none.
   */
  protected function getTermAllIndexedParents($tid) {
    $query = $this->connection->select('taxonomy_parents_index', 'c');
    $query->fields('c', ['ptid']);
    $query->distinct(TRUE);

    if (is_array($tid)) {
      $query->condition('c.tid', $tid, 'IN');
    }
    else {
      $query->condition('c.tid', $tid, '=');
    }
    return $query->execute()->fetchCol(0);
  }

  /**
   * Returns all children Taxonomy Term IDs of a given Taxonomy Term.
   *
   * @param \Drupal\taxonomy\TermInterface|null $term
   *   The given Taxonomy Term ID.
   *
   * @return int[]
   *   Array of children Taxonomy Term IDs. Empty array if there's none.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getAllChildren(?TermInterface $term) {
    $vid = $term->bundle();
    $tid = $term->id();

    // 1 to get only immediate children, NULL to load entire tree.
    $depth = 4;

    // True will return loaded entities rather than ids.
    $load_entities = FALSE;

    $childrenObjects = $this->getTermStorage()->loadTree($vid, $tid, $depth, $load_entities);

    $childrenIds = [];
    foreach ($childrenObjects as $childrenObject) {
      if (isset($childrenObject->tid)) {
        $childrenIds[$childrenObject->tid] = $childrenObject->tid;
      }
    }

    $childrenIds[$tid] = $tid;

    return $childrenIds;
  }

}
