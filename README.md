# Taxonomy Parents Index


When building a view for listing content that contain a specific Taxonomy Term or the Term's children terms, the core provides a special contextual filter: "Has taxonomy term ID (with depth)". As the Taxonomy Vocabulary grows over time, that contextual filter causes slowdowns on those view pages.


This module's aim is to replace the slow contextual filter with a faster solution: by indexing all parent Term IDs in a separate taxonomy_parents_index table (along with the Term's own ID).


One drawback of this solution is that it needs to maintain the records in the separate database table every time a Taxonomy Term is created, updated or deleted.


## Usage

- After installing a module, clear caches.
- Make sure to have a few Taxonomy Terms.
- Go to /admin/taxonomy_term_parents_reindex and click to 'Reindex' to index the parents in the custom table.


To replace the "Has taxonomy term ID (with depth)" contextual filter in the Taxonomy term (Content) View, you need to:

1. Remove it
2. Add a relationship called "Join from taxonomy_index to taxonomy_parents_index"
3. Tick the checkbox at "Require this relationship" and click to Apply
4. Add the "Parent ID" contextual filter (it under Category "Taxonomy Parents Index") and click to Apply
5. At the contextual filter settings under "WHEN THE FILTER VALUE IS NOT IN THE URL" click to "Provide default value" and set it to "Taxonomy term ID from URL" and click to Apply.
Save the view.

Now we replaced the contextual filter, so we need to fix the view title:

- In the view's header settings click to "Global: Rendered entity - Taxonomy term (Global: Rendered entity - Taxonomy term)".
- Set the "Taxonomy term ID" to "{{ raw_arguments.ptid }}" and click to Apply
- Save the view.
